/* Function Reduction such that {Reduction [a0 a1 a2 a3 a4 a5 ...] A B C D} = A*a0+B*a1+C*a2+D*a3+A*a4+ */

declare
fun {Reduction L A B C D}
    local LAC LBD LA LB LC LD 
        fun {SumL List}
		{FoldL List fun {$ X Y} X + Y end 0}
	end
    in
        {Split L LAC LBD}
        {Split LAC LA LC}
        {Split LBD LB LD}
        A * {SumL LA} + B * {SumL LB} +
        C * {SumL LC} + D * {SumL LD}
    end
end