/* Procedure Split such that {Split [a0 a1 a2 a3 ...] L1 L2} binds L1=[a0 a2 ...] and L2=[a1 a3 ...] */

declare
proc {Split L L1 L2}
    case L of
        nil then L1 = nil L2 = nil
    []  H|nil then L1 = [H] L2 = nil
    []  H1|H2|T then 
        local T1 T2 in
            L1 = H1 | T1
            L2 = H2 | T2
            {Split T T1 T2}
        end
    end
end