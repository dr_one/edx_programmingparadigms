declare
fun {Prefix L1 L2}
	case L1 of
		nil then true
		[] H|T then L2 \= nil andthen H == L2.1 andthen {Prefix T L2.2}
	end
end

fun {FindString L1 L2}
	case L2 of
		nil then L1 == nil
		[] H|T then {Prefix L1 L2} orelse {FindString L1 T}
	end
end