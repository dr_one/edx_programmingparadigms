/* In this exercise, you are asked to produce a list containing the n first factorial number */

declare
fun {Fact N}
	fun {Aux Length Last}
		if N == Length - 1 then nil
		else Length * Last | {Aux Length + 1 Length * Last}
		end 
	end
	in 
	{Aux 1 1}
end