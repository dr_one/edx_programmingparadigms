/*you are asked to flatten a list. The idea is that some lists can contain lists in them. 
The Flatten function takes a list L as input and returns L with all the elements contained in it, but without any list in it. 
For instance, {Flatten [[1] [2] [3]]} returns [1 2 3].*/

declare
fun {FlattenList L}
	case L of
		nil then nil
		[] (H|HT)|T then {Append {FlattenList H|HT} {FlattenList T}}
		[] H|T then H | {FlattenList T}
	end
end