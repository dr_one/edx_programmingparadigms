/* You are asked to provide the body of a function (named Build) that returns another function defined by a domain D and a codomain C given as input. 
Here is the signature: fun {Build D C}
D is a list representing the domain of your output function and C is a list representing the codomain. 
D and C have the same length and do not contain only integers, but may contain atoms or lists. 
If your output function is named f then f(D[i]) = C[i]. This means that if we call your output function with the third element of D then 
the output has to be the third element of C. If we call your output function with an element that is not present in D, then 
you have to return "bottom" (without the quotes). */

declare
fun {Build D C}
	fun {Aux X D C}
		case D of
			nil then 'bottom'
		[]	H|T andthen H == X then C.1
		[]	H|T then {Aux X T C.2}
		end
	end
	in
	fun {$ X} {Aux X D C} end
end