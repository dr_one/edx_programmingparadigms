/* You are asked to provide the body of the function Transform, which takes a list and returns a record. The list contains the components to build the record. Here is the function signature:
                fun {Transform L}

The list (named L) always contains three elements:

The first element is the label of the record;
The second element is a list containing the features of the record;
The third element is a list, which has the same length as the second element, containing the values stored in the fields of the record.
The nth element of the L second element is the field name corresponding to the field value represented by the nth element of the L third element. This explains why the two lists have the same length. For instance, from [z [3 a] [b 5]], Transform produces z(3:b a:5).

Be careful! If one of the field values (third element) is a list, then this list has the same format as L and has also to be transformed into a record! */


declare
fun {Transform L}
	fun {Assign R Fields Values}
		case Fields of
			nil then R
		[] H|T then R.H = {Transform Values.1} {Assign R Fields.2 Values.2}
		end
	end
	in
	case L of
		Label|Fields|Values|nil then {Assign {Record.make Label Fields} Fields Values}
		else L
	end
end


/* Tests */
Plain = ['Plain' [a b c] [1 2 3]]
{Browse {Transform Plain}}

Nested2 = ['Nested2' [a b c] [1 2 3]]
Nested1 = ['Nested1' [g h j] [6 Nested2 8]]
Root = ['Root' [q w e r t] [9 Nested1 8 Nested2] 0]
Plain = ['Plain' [a b c] [1 2 3]]
{Browse {Transform Root}}