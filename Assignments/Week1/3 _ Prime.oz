/* Use accumulators in order to determine if a number is prime. */

fun {Prime N}
	local IsPrime in 
		fun {IsPrime N P}
			if P == 1 then true
			elseif N == 1 orelse N mod P == 0 then false
			else {IsPrime N P - 1} 
			end
		end
		{IsPrime N N - 1}
	end
end