/* Using accumulators allows you to keep a stack constant size during the recursion calls. 
Use accumulators to reverse an integer. For instance, given 1234, you are asked to return 4321. Do not consider integers with zeros. */

fun {MainMirror Int}
	local Mirror in
		fun {Mirror Int Acc}
			if Int == 0 then Acc
			else {Mirror (Int div 10) (Acc * 10 + Int mod 10)}
			end
		end
		{Mirror Int 0}
	end
end