/* Use an accumulator to compute the sum of the square of the n first integers. */

fun {MainSum N}
	local Sum in
		fun {Sum N Acc}
			if N == 0 then Acc
			else {Sum N-1 N*N + Acc}
			end
		end
		{Sum N 0}
	end
 end