/* A tree is balanced if its two subtrees have the same amount of leaves (with a difference of maximum 1) and are balanced themselves. A leaf is a balanced tree. */

declare
fun {IsBalanced Tree}
	case Tree of
		leaf then true
	[]	btree(Root left:L right:R) then 
				{IsBalanced L} andthen 
				{IsBalanced R} andthen 
				{Number.abs {NumLeaves L} - {NumLeaves R}} =< 1 
   end
end