/* Counts the number of leaves in a tree */

declare 
fun{NumLeaves Tree}
	case Tree of
		leaf then 1
	[]  btree(Root left:L right:R) then {NumLeaves L} + {NumLeaves R}
	end
end