/* Returns a list containing the elements of the tree following the infix order */

declare
fun {Infix Tree}
	case Tree of
		leaf then nil
		[] btree(Root left:L right:R) then
			local Left = {Infix L} Right = {Infix R} in
				{Append {Append Left [Root]} Right}
			end
	end
end

/* Tests */

Tree = btree(4 
	left:btree(2 left:btree(1 left:leaf right:leaf)
                                  right:btree(3 left:leaf right:leaf))
        right:btree(5 left:leaf right:leaf))
{Browse {Infix Tree} == [1 2 3 4 5]}