/* SortWithTree part 1: function has to build an ordered tree from a non-ordered list */

declare
fun {FromListToTree L}
	fun {InsertElement Tree Value}
		case Tree of
			leaf then btree(Value left:leaf right:leaf)
		[]  btree(Root left:L right:R) then
				if Root > Value then btree(Root left:{InsertElement L Value} right:R)
				elseif Root < Value then btree(Root left:L right:{InsertElement R Value})
				else btree(Root left:L right:R)
			end
		end
	end
	
	fun {InsertList Tree List}
		case List of
			nil then Tree
		[]	H|T then {InsertList {InsertElement Tree H} T}
		end
	end
	
	in
	
	{InsertList leaf L}
end